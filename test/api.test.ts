import request from "supertest";
import app from "../src/app";
import {container} from "tsyringe";
import {DBService, UserService} from "../src/services"
const dbService = container.resolve(DBService);
const userService = container.resolve(UserService);
import {expect} from "chai";

beforeEach(async () => {
    await dbService.clearAll();
});

describe("GET /api/v1/user/:id", () => {
    it("should return 200 OK", async (done) => {
        let user = await userService.createUser({
            email: 'test@user.com',
            password: 'sdsDedfd',
            firstName: 'John',
            lastName: 'Doe'
        });
        const res = await request(app)
            .get(`/api/v1/user/${user._id}`)
            .expect(200);
        expect(res.body).property('email', 'test@user.com');
        done()
    });
});

describe("POST /api/v1/user", () => {
    it("should return 200 OK", async (done) => {
        const res = await request(app)
            .post(`/api/v1/user`).send({
                email: 'test@user.com',
                password: 'sdsDedfd',
                firstName: 'John',
                lastName: 'Doe'
            })
            .expect(200);
        expect(res.body).property('email', 'test@user.com');
        done()
    });
});
