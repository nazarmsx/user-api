"use strict";
import * as express from 'express'
import bcrypt from 'bcrypt';

const router: express.Router = express.Router();
import {check, sanitize, validationResult} from "express-validator";
import {container} from "tsyringe";
import {UserService} from "../services"

const userService = container.resolve(UserService);
import {Response, Request, NextFunction} from "express";

router.get('/api/v1/user/:id', async function (req: Request, res: Response, next: NextFunction) {
    const userId = req.params.id;
    const user = await userService.findUserById(userId);
    if (!user) {
        res.status(404).json({error: 'USER_NOT_FOUND'});
    }
    delete user.password;
    res.json(user);
});

async function createUser(req: Request, res: Response, next: NextFunction) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({error: "BAD_PARAMETERS", errors: errors.array()});
    }
    const user: any = {
        email: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        city: req.body.city ? req.body.city : null
    };
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(req.body.password, salt);
    user.salt = salt;

    try {
        const createdUser = await userService.createUser(user);
        delete createdUser.salt;
        delete createdUser.password;
        res.json(createdUser)
    } catch (e) {
        return res.status(400).json({error: e.message})
    }

}

router.post('/api/v1/user', [
    check("email", "email is required").not().isEmpty().isEmail(),
    check("password", "password is required").not().isEmpty(),
    check("password", "password length should be 8").not().isEmpty().isLength({min: 8, max: 8}),
    check("password", "password  should have capital letter").not().isEmpty().matches(/[a-zA-Z]/, "g"),
    check("firstName", "email is required").not().isEmpty().isLength({max: 25}),
    check("lastName", "email is required").not().isEmpty().isLength({max: 25}),
], createUser);

async function updateUser(req: Request, res: Response, next: NextFunction) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({error: "BAD_PARAMETERS", errors: errors.array()});
    }
    const user: any = {
        email: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        city: req.body.city ? req.body.city : null
    };
    if (req.body.password) {
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(req.body.password, salt);
        user.salt = salt;
    }

    try {
        const updatedUser = await userService.updateUser(req.body.id, user);
        if (!updatedUser) {
            return res.status(404).json({error: "USER_NOT_FOUND"})
        }

        delete updatedUser.salt;
        delete updatedUser.password;
        res.json(updatedUser)
    }catch (e) {
        return res.status(400).json({error: e.message})
    }

}

router.put('/api/v1/user', [
    check("email", "email is required").not().isEmpty().isEmail(),
    check("password", "password is required").optional().not().isEmpty(),
    check("password", "password length should be 8").optional().not().isEmpty().isLength({min: 8, max: 8}),
    check("password", "password  should have capital letter").optional().not().isEmpty().matches(/[a-zA-Z]/, "g"),
    check("id", "id is required").not().isEmpty(),
    check("firstName", "email is required").not().isEmpty().isLength({max: 25}),
    check("lastName", "email is required").not().isEmpty().isLength({max: 25})
], updateUser);

router.delete('/api/v1/user/:id', async function (req: Request, res: Response, next: NextFunction) {
    const id = req.params.key;
    let entry = await userService.deleteUser(id);
    if (!entry) {
        return res.status(404).json({error: "USER_NOT_FOUND"})
    }
    res.json({message: "User successfully deleted."})
});

export {router};