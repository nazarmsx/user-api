import mongoose from "mongoose";

export type IUser = {
    email: string;
    password: string;
    firstName: string;
    lastName: string,
    city?: string;
    salt?: string;
    _id?: string;
}

export type UserDocument = mongoose.Document & IUser ;

const userSchema = new mongoose.Schema({
    email: {type: String, unique: true},
    password: String,
    salt: String,
    firstName: String,
    lastName: String,
    city: String,
});

export const User = mongoose.model<UserDocument>("users", userSchema);
