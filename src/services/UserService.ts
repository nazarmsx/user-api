import {injectable} from "tsyringe";
import {User, UserDocument, IUser} from "../models";
import to from 'await-to-js';
import logger from "../util/logger";
import bcrypt from "bcrypt";

@injectable()
export class UserService {

    constructor() {
    }

    public async createUser(user: IUser): Promise<IUser> {
        const inDbUser = await User.findOne({email: user.email});
        if (inDbUser) {
            throw new Error("DUPLICATE_EMAIL");
        }
        const userDoc = new User(user);
        try {
            let res = await userDoc.save();
            return res.toObject();

        } catch (error) {
            throw error;
        }
    }

    public async findUserById(userId: string): Promise<UserDocument> {
        return  User.findOne({_id: userId});
    }

    public async updateUser(id: string, user: IUser) {
        let userDoc = await this.findUserById(id);
        if (!userDoc) {
            throw new Error('USER_NOT_FOUND');
        }
        Object.assign(userDoc, user);
        const updatedUser = await userDoc.save();
        return updatedUser.toObject();
    }

    public async deleteUser(userId: string) {
        return User.findOneAndDelete({id: userId});
    }
}