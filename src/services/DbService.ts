import {injectable} from "tsyringe";
import {User} from "../models";

@injectable()
export class DBService {

    constructor() {
    }

    public async clearAll(): Promise<any> {
        return Promise.all([User.deleteMany({})])
    }
}